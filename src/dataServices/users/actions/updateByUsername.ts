import { Users } from '../../../submodules/shared-library/models/Users';
import { IUser, UserScope } from '../../../submodules/shared-library/interfaces/IUser';

const updateByUsername = async (username: string, update: any): Promise<IUser> => {
    return Users.findOneAndUpdate({username, scope: UserScope.DASHBBOARD}, update, {new: true});
};

export { updateByUsername };