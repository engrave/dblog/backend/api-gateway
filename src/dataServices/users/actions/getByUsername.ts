import { Users } from '../../../submodules/shared-library/models/Users';
import { UserScope } from '../../../submodules/shared-library/interfaces/IUser';

const getByUsername = async (username: string) => {
    return Users.findOne({username, scope: UserScope.DASHBBOARD});
};

export { getByUsername };