import { Domains } from '../submodules/shared-library/models/Domains';
import { IDomain } from '../submodules/shared-library/interfaces/IDomain';

export class DomainsDataService {
    public static getByQuery = async (query: any): Promise<IDomain> => Domains.findOne(query);
    public static getByUsername = async (username: string): Promise<IDomain[]> => Domains.find({owner: username});
    public static getById = async (id: string): Promise<IDomain> => Domains.findOne({_id: id});
    public static create = async (domain: string, username: string, purchasePrice: any): Promise<IDomain> => Domains.create({
        owner: username,
        domain,
        purchasePrice,
        paid: false
    })
    public static remove = async (id: string): Promise<any> => Domains.deleteOne({_id: id});
}
