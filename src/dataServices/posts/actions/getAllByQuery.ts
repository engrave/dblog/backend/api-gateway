import { Posts } from "../../../submodules/shared-library/models/Posts";

const getAllByQuery = async (query: any) => {
    return await Posts.find(query);
}

export { getAllByQuery };