import { Posts } from "../../../submodules/shared-library/models/Posts";

const getSingle = async (query: any) => {
    return await Posts.findOne(query);
}

export { getSingle };