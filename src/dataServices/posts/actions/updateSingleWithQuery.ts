import { Posts } from "../../../submodules/shared-library/models/Posts";
import { IPost } from "../../../submodules/shared-library/interfaces/IPost";

const updateSingleWithQuery = async (id: string, query: any): Promise<IPost> => {
    return Posts.updateOne({_id: id}, {
        $set: query
    });
}

export { updateSingleWithQuery };