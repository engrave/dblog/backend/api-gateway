import { Posts } from "../../../submodules/shared-library/models/Posts";

const removeWithQuery = async (query: any) => {
    return await Posts.deleteOne(query);
}

export { removeWithQuery };