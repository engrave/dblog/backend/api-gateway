import { Posts } from '../../../submodules/shared-library/models/Posts';

const removeByBlogId = async (id: string) => {
    return Posts.deleteMany({blogId: id});
};

export { removeByBlogId };
