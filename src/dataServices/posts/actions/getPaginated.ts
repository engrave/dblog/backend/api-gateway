import { Posts } from "../../../submodules/shared-library/models/Posts";

const DEFAULT_LIMIT = 10;
const DEFAULT_SKIP = 0;
const DEFAULT_SORT_BY = 'updatedAt';
const DEFAULT_SORT_ORDER = 'asc';

const getMultipleWithQueryPaginated = async (query: any, page?: number, quantity?: number, sortBy?: string, order?: string) => {

    const limit = quantity ? quantity : DEFAULT_LIMIT;
    const skip = page ? page * limit : DEFAULT_SKIP;
    const sort = sortBy ? sortBy : DEFAULT_SORT_BY;
    const sortOrder: number = order === DEFAULT_SORT_ORDER ?  1 : -1;

    const result = await Posts.aggregate([
        { $match: query },
        { $sort: { [sort]: sortOrder } },
        {
            $facet: {
                total: [{ $count: "count" }],
                data: [{ $skip: skip }, { $limit: limit }]
            }
        },
        {
            $addFields: {
                metadata: {
                    total: { $ifNull: [ { $arrayElemAt: ["$total.count", 0] }, 0] },
                    page: skip / limit,
                    pages: { $ifNull: [{ $ceil: { $divide: [{   $arrayElemAt: ["$total.count", 0] }, limit] } }, 0] },
                    limit: limit
                }
            }
        },
        {
            $project: {
                metadata: 1,
                data: {
                    _id: 1,
                    blogId: 1,
                    categories: 1,
                    tags: 1,
                    username: 1,
                    title: 1,
                    permlink: 1,
                    updatedAt: 1,
                    createdAt: 1,
                    publishedAt: 1,
                    thumbnail_image: 1,
                    hidden: 1
                }
            }
        }
    ]);

    return result[0];
}

export { getMultipleWithQueryPaginated };