import { Posts } from "../../../submodules/shared-library/models/Posts";

const createWithQuery = async (query: any) => {
    return await Posts.create(query);
}

export { createWithQuery };