import axios, { AxiosRequestConfig } from 'axios';
import { handleServiceError, logger, microservices } from '../../submodules/shared-library';

export default async (domain: string) => {

    try {
        return await handleServiceError( async () => {
            const options: AxiosRequestConfig = {
                method: 'POST',
                data: {
                    domain
                },
                url: `http://${microservices.ssl}/domain/validate`
            };

            const response = await axios(options);

            const { is_pointing } = response.data;

            return is_pointing;
        });
    } catch (e) {
        logger.error(e, "Error validating domain");
        return false;
    }

};
