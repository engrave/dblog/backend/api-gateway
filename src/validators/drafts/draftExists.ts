import { PostStatus } from '../../submodules/shared-library/enums/PostStatus';
import { getSingle } from '../../dataServices/posts';

export async function draftExists(id: any) {
    return await getSingle({
        _id: id, 
        status: PostStatus.DRAFT
    });
}