import { DomainsDataService } from '../../dataServices/DomainsDataService';
import { IDomain } from '../../submodules/shared-library/interfaces/IDomain';

export async function domainReservationExists(id: any): Promise<IDomain> {
    return DomainsDataService.getById(id);
}
