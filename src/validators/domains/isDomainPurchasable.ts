import { customValidatorError } from '../../submodules/shared-library/utils';

export async function isDomainPurchasable(domainDetails: any): Promise<any> {
    if (!domainDetails.purchasable) { return customValidatorError('body', 'domain', `${domainDetails.sld}.${domainDetails.tld}`, 'Domain unavailable'); }
    return true;
}
