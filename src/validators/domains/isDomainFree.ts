import { PostStatus } from '../../submodules/shared-library/enums/PostStatus';
import { getSingle } from '../../dataServices/posts';
import { DomainsDataService } from '../../dataServices/DomainsDataService';

export async function isDomainFree(domain: string): Promise<any> {
    const details = await DomainsDataService.getByQuery({domain});
    return !details;
}
