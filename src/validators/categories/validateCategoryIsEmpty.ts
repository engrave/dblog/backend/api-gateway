import { getLatestFromCategory } from "../../submodules/shared-library/services/cache/cache";
import { ICategory } from "../../submodules/shared-library/interfaces/ICategory";
import { getMultipleWithQueryPaginated } from "../../dataServices/posts";
import { PostStatus } from "../../submodules/shared-library";

export async function validateCategoryIsEmpty(category: ICategory) {

    const posts = await getLatestFromCategory(category._id, category.blogId, 0, 1);
    const drafts = await getMultipleWithQueryPaginated({categories: { $in: [category._id.toString()]} });

    return (posts.length === 0 && drafts.metadata.total === 0 );

}