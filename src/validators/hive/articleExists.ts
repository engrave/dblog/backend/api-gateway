import { getHiveArticle } from '../../submodules/shared-library/services/hive/hive';

export async function articleExists(username: string, permlink: string): Promise<boolean> {
    try {
        return await getHiveArticle(username, permlink);
    } catch (error) {
        return false;
    }

}
