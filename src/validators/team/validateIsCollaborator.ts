import blogsService from "../../services/blogs/services.blogs";
import { customValidatorError } from "../../submodules/shared-library";

export async function validateIsCollaborator(blogId: string, username: string) {

    const blog =  await blogsService.getBlogByQuery({_id: blogId});

    const isCollaborator = blog.collaborators.find( member => member.username === username);

    if(!isCollaborator) return await customValidatorError('body', 'username', username, "This user is not a collaborator"); 
        
    return true;

}