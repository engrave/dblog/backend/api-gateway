import blogsService from "../../services/blogs/services.blogs";
import { customValidatorError } from "../../submodules/shared-library";

export async function validateIsNotCollaborator(blogId: string, username: string) {

    const blog =  await blogsService.getBlogByQuery({_id: blogId});

    const isOwner = blog.owner === username;
    const isCollaborator = blog.collaborators.find( member => member.username === username);

    if(isOwner || isCollaborator) return await customValidatorError('body', 'username', username, "This user is already a collaborator"); 
        
    return true;

}