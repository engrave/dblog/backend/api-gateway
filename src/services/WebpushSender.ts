/* tslint:disable:variable-name */
import { handleServiceError } from '../submodules/shared-library/hof';
import axios, { AxiosRequestConfig } from 'axios';
import { microservices } from '../submodules/shared-library/config';
import { getCoinsPrice } from './statistics';

export class WebpushSender {

    public static push = (
        blog_title: string,
        article_title: string,
        article_url: string,
        image_url: string,
        api_key: string,
        app_id: string): Promise<any> => {
        return handleServiceError(async () => {
            const options: AxiosRequestConfig = {
                url: `http://${microservices.webpush_sender}/onesignal`,
                method: 'POST',
                data: {
                    blog_title,
                    article_title,
                    article_url,
                    image_url,
                    api_key,
                    app_id
                }
            };

            const {data} = await axios(options);

            return data;
        });
    }
}
