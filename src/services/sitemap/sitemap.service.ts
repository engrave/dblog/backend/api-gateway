import rebuildSitemap from './actions/rebuildSitemap';

const sitemap = {
    rebuildSitemap
}

export default sitemap;