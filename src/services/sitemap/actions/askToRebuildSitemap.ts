import { handleServiceError } from "../../../submodules/shared-library";
import axios, { AxiosRequestConfig } from 'axios';

export default async (domain: string) => {

    return handleServiceError(async () => {
    
        const options: AxiosRequestConfig = {
            url: "http://sitemap-builder:3000/sitemap",
            method: 'POST',
            data: {
                domain: domain
            }
        };
    
        const { data } = await axios(options);
    
        return data;
    
    })

}