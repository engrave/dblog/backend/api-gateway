import getLatestByCategory from './actions/getLatestByCategory';
import addPublishedPost from './actions/addPublishedPost';

const publishedService = {
    getLatestByCategory,
    addPublishedPost
}

export default publishedService;