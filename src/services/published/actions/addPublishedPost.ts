import { PublishedArticles } from "../../../submodules/shared-library/models/Published";
import { IBlog } from "../../../submodules/shared-library/interfaces/IBlog";

const striptags = require('striptags');

const addPublishedPost = async (article: any, blog: IBlog) => {

    const post = await PublishedArticles.create({
        blogId: blog._id,
        title: article.title,
        abstract: striptags(article.body.substr(0, 250)),
        image: article.thumbnail_image,
        username: article.username,
        steemit_permlink: `https://peakd.com/@${article.username}/${article.permlink}`,
        engrave_permlink: `https://${ blog.custom_domain ? blog.custom_domain : blog.domain}/${article.permlink}`,
        content_category: blog.content_category
    });

    return post;
}

export default addPublishedPost
