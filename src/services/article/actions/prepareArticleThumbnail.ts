import { PostStatus } from '../../../submodules/shared-library';

const getUrls = require('get-urls');
const isImage = require('is-image');

const prepareArticleThumbnail = (body: string): string => {

    const urls = getUrls(body);

    for (const url of urls) {
        let link = url;

        if (url[url.length - 1] === ')') {
            link = url.substring(0, url.length - 1);
        }

        if (isImage(link)) {
            return link;
        }
    }

    return null;
};

export { prepareArticleThumbnail };
