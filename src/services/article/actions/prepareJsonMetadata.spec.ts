import { describe, it } from 'mocha';
import { expect } from 'chai';

import prepareJsonMetadata from './prepareJsonMetadata';
import exampleArticles from './test-assets/exampleArticles';
import exampleBlogs from './test-assets/exampleBlogs';

const url = 'https://string/test-permlink';

describe('prepareJsonMetadata', async () => {
    it('should find valid mention', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withValidMention, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: [],
            image: [],
            links: [],
            users: ['@engrave'],
            canonical_url: url,
            engrave: {
                categories: [],
                custom_domain: 'string',
                domain: 'string',
                featured_image: null,
                permlink: 'test-permlink',
                url
        }
        });
    });

    it('should not find invalid mention', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withInvalidMention, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: [],
            image: [],
            links: [],
            users: [],
            canonical_url: url,
            engrave: {
                categories: [],
                custom_domain: 'string',
                domain: 'string',
                featured_image: null,
                permlink: 'test-permlink',
                url
            }
        });
    });

    it('should find valid link', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withLink, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: [],
            image: [],
            links: ['https://example.org'],
            users: [],
            canonical_url: url,
            engrave: {
                categories: [
                  'test-category'
                ],
                custom_domain: 'string',
                domain: 'string',
                featured_image: null,
                permlink: 'test-permlink',
                url
            }
        });
    });

    it('should find valid image', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withImage, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: [],
            image: ['https://example.com/featured-image.png', 'https://test.imgur.com/test.jpg'],
            links: [],
            users: [],
            canonical_url: url,
            engrave: {
                categories: [],
                custom_domain: 'string',
                domain: 'string',
                featured_image: 'https://example.com/featured-image.png',
                permlink: 'test-permlink',
                url
            }
        });
    });

    it('should find valid tags', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withTags, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: ['first-tag', 'second-tag'],
            image: [],
            links: [],
            users: [],
            canonical_url: url,
            engrave: {
                categories: [],
                custom_domain: 'string',
                domain: 'string',
                featured_image: null,
                permlink: 'test-permlink',
                url
            }
        });
    });

    it('should find link, image and mention', async () => {

        const result = await prepareJsonMetadata(exampleArticles.withMentionImageAndLink, exampleBlogs[0]);

        expect(result).to.deep.equal({
            format: 'markdown',
            app: 'engrave',
            tags: [],
            image: ['https://example.com/featured-image.png', 'https://test.imgur.com/test.jpg'],
            links: ['https://example.org'],
            users: ['@engrave'],
            canonical_url: url,
            engrave: {
                categories: [],
                custom_domain: 'string',
                domain: 'string',
                featured_image: 'https://example.com/featured-image.png',
                permlink: 'test-permlink',
                url
            }
        });
    });
});
