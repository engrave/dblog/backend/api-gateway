import { IBlog } from '../../../submodules/shared-library/interfaces/IBlog';
import prepareJsonMetadata from './prepareJsonMetadata';
import { OperationsScope } from '../../../submodules/shared-library';
import { IPost } from '../../../submodules/shared-library/interfaces/IPost';
import { IUser } from '../../../submodules/shared-library/interfaces/IUser';
import appendEngraveInfo from '../../../submodules/shared-library/utils/appendEngraveInfo';

export default async (article: IPost, scope: OperationsScope, blog: IBlog, user: IUser) => {

    const json_metadata = await prepareJsonMetadata(article, blog);

    const operations: any[] = [
        ['comment',
            {
                parent_author: '',
                parent_permlink: article.parent_category ? article.parent_category : article.tags[0],
                author: article.username,
                permlink: article.permlink,
                title: article.title,
                body: appendEngraveInfo(article.body, article.permlink, blog),
                json_metadata: JSON.stringify(json_metadata)
            }
        ]
    ];

    switch (scope) {

        case OperationsScope.REMOVE:
            return [
                ['delete_comment', {
                    author: article.username,
                    permlink: article.permlink
                }]];

        case OperationsScope.EDIT:
        return operations;

        case OperationsScope.PUBLISH:

        let extensions: any = [];

        if (process.env.NODE_ENV !== 'staging') {

            if (!user.adopter) {
                extensions = [
                    [0, {
                        beneficiaries: [
                            { account: 'engrave', weight: 100 } // 1% beneficiary
                        ]
                    }]
                ];
            }

        }

        operations.push(
            ['comment_options', {
                author: article.username,
                permlink: article.permlink,
                max_accepted_payout: article.decline_reward ? '0.000 HBD' : '1000000.000 HBD',
                percent_hbd: 10000,
                allow_votes: true,
                allow_curation_rewards: true,
                extensions
            }
            ]);

        return operations;

        default:
        throw new Error('Invalid operations scope');
    }
};
