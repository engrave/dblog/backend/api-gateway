import { IJsonMetadata as IJsonMetadata } from '../../../submodules/shared-library/interfaces/IJsonMetadata';
import { IBlog } from '../../../submodules/shared-library/interfaces/IBlog';
import { IPost } from '../../../submodules/shared-library/interfaces/IPost';

const getUrls = require('get-urls');
const isImage = require('is-image');

export default async (article: IPost, blog: IBlog): Promise<IJsonMetadata> => {

    const url = `https://${blog.custom_domain ? blog.custom_domain : blog.domain}/${article.permlink}`;
    const json_metadata = {
        format: 'markdown',
        app: 'engrave',
        tags: article.tags,
        image: prepareImages(article),
        links: prepareLinks(article),
        users: prepareMentions(article.body),
        canonical_url: url,
        engrave: {
            domain: blog.domain,
            custom_domain: blog.custom_domain,
            permlink: article.permlink,
            url: url,
            featured_image: article.featured_image ? article.featured_image : null,
            categories: article.categories
        }
    };

    return json_metadata;
};

const prepareMentions = (body: string): string[] => {
    const usernames = body.match(/@[a-z](-[a-z0-9](-[a-z0-9])*)?(-[a-z0-9]|[a-z0-9])*(?:\.[a-z](-[a-z0-9](-[a-z0-9])*)?(-[a-z0-9]|[a-z0-9])*)*/g);
    return uniq(usernames);
};

const prepareLinks = (article: IPost): string[] => {

    const links: string[] = [];

    const urls: string[] = getUrls(article.body);

    urls.forEach((url) => {

        let link = url;

        if (url[url.length - 1] == ')') {
            link = url.substring(0, url.length - 1);
        }

        if (!isImage(link)) {
            links.push(link);
        }
    });

    return links;
};

const prepareImages = (article: IPost): string[] => {

    const images: string[] = [];

    if (article.featured_image) {
        images.push(article.featured_image);
    }

    const urls: string[] = getUrls(article.body);

    urls.forEach((url) => {

        let link = url;

        if (url[url.length - 1] == ')') {
            link = url.substring(0, url.length - 1);
        }

        if (isImage(link)) {
            images.push(link);
        }
    });

    return images;
};

const uniq = (a: string[]) => [...new Set(a)];
