import { PostStatus } from '../../../submodules/shared-library';
import { prepareArticleThumbnail } from './prepareArticleThumbnail';

const prepareArticleFromRequest = (reqBody: any, username: string, blogId: string): any => {
    return {
        blogId,
        username,
        author: username,
        scheduledAt: null,
        publishedAt: new Date(),
        title: reqBody.title,
        body: reqBody.body,
        categories: reqBody.categories,
        tags: reqBody.tags,
        featured_image: reqBody.featured_image,
        thumbnail_image: reqBody.featured_image ? reqBody.featured_image : prepareArticleThumbnail(reqBody.body),
        status: PostStatus.PUBLISHED,
        decline_reward: reqBody.decline_reward,
        permlink: reqBody.permlink,
        parent_category: reqBody.tags[0]
    };
};

export { prepareArticleFromRequest };
