import prepareJsonMetadata from './actions/prepareJsonMetadata';
import {prepareArticleFromRequest} from './actions/prepareArticleFromRequest';

const article = {
    prepareJsonMetadata,
    prepareArticleFromRequest
}

export default article;