import { handleServiceError, microservices } from "../../../submodules/shared-library";
import axios, { AxiosRequestConfig } from "axios";

const removeNginxConfiguration = async (domain: string) => {
    return handleServiceError( async () => {
        const options: AxiosRequestConfig = {
            url: `http://${microservices.nginx_configurator}/configuration/remove`,
            method: 'POST',
            data: { domain }
        }

        const { data } = await axios(options);
        return data;
    });
}

export { removeNginxConfiguration };
