import { uploadFromUrl } from './actions/uploadFromUrl';
import { uploadFromFile } from './actions/uploadFromFile';

export { uploadFromUrl };
export { uploadFromFile };

export default {
    uploadFromUrl,
    uploadFromFile
}