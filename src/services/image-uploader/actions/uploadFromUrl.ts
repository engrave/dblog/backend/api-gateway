import { microservices } from "../../../submodules/shared-library";
import axios, { AxiosRequestConfig } from 'axios';

const uploadFromUrl = async (url: string) => {
    const options: AxiosRequestConfig = {
        url: `http://${microservices.image_uploader}/image/upload`,
        method: 'POST',
        data: {
            url
        }
    }

    const { data } = await axios(options);
    return data;
}

export { uploadFromUrl };
export default uploadFromUrl;
