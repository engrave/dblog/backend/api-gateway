import { microservices } from "../../../submodules/shared-library";
import axios, { AxiosRequestConfig } from 'axios';
const FormData = require('form-data')

const uploadFromFile = async (file: any) => {

    let formData = new FormData();
    formData.append('file', file.buffer, file.originalname);

    const options: AxiosRequestConfig = {
        url: `http://${microservices.image_uploader}/image/upload`,
        method: 'POST',
        data: formData,
        headers: formData.getHeaders()
    }
    const { data: {link} } = await axios(options);
    return link;
}

export { uploadFromFile };
export default uploadFromFile;
