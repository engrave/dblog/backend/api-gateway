import { getSingle } from '../../../dataServices/posts' 
import { PostStatus } from '../../../submodules/shared-library';

async function validatePermlinkIsNotTaken(blogId: string, permlink: string) {

    const post = await getSingle({blogId, permlink, status: PostStatus.PUBLISHED})

    if(post) throw new Error("Article with this title already exists on this blog!");
    
}

export default validatePermlinkIsNotTaken;