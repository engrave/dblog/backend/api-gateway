import { Posts } from "../../../submodules/shared-library/models/Posts";

async function validatePostOwnership(id: string, username: string) {

    if(!id) return null;

    const post = await Posts.findById(id);

    if (post.username != username) throw new Error("You are not the owner of that post!");

    return post;
}

export default validatePostOwnership;
