import validatePostOwnership from './actions/validatePostOwnership';
import validatePermlinkIsNotTaken from './actions/validatePermlinkIsNotTaken';

const postsService = {
    validatePostOwnership,
    validatePermlinkIsNotTaken
}

export default postsService;