import { Categories } from "../../../submodules/shared-library/models/Categories";
import { ICategory } from "../../../submodules/shared-library/interfaces/ICategory";

async function updateWithQuery(id: any, query: ICategory) {
    await Categories.updateOne({_id: id}, {
        $set: query
    });

    return await Categories.findById(id);
}

export default updateWithQuery;