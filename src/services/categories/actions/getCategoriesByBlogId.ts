import { Categories } from "../../../submodules/shared-library/models/Categories";
import { ICategory } from "../../../submodules/shared-library/interfaces/ICategory";

async function getCategoriesByBlogId(blogId: string): Promise<ICategory[]> {
    return await Categories.find({blogId: blogId});
}

export default getCategoriesByBlogId;