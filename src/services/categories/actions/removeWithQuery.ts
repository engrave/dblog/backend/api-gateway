import { Categories } from "../../../submodules/shared-library/models/Categories";

async function removeWithQuery(query: any) {
    return await Categories.deleteOne(query);
}

export default removeWithQuery;