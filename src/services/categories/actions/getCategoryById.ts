import { Categories } from "../../../submodules/shared-library/models/Categories";
import { ICategory } from "../../../submodules/shared-library/interfaces/ICategory";

async function getCategoryById(id: string): Promise<ICategory> {
    return await Categories.findById(id);
}

export default getCategoryById;