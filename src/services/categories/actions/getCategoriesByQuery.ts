import { Categories } from "../../../submodules/shared-library/models/Categories";
import { ICategory } from "../../../submodules/shared-library/interfaces/ICategory";

async function getCategoriesByQuery(query: any): Promise<ICategory[]> {
    return await Categories.find(query);
}

export default getCategoriesByQuery;