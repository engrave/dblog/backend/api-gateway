import { Categories } from "../../../submodules/shared-library/models/Categories";
import { ICategory } from "../../../submodules/shared-library/interfaces/ICategory";

async function createCategoryWithQuery(query: any): Promise<ICategory> {
    return await Categories.create(query);
}

export default createCategoryWithQuery;