import { Blogs } from "../../../submodules/shared-library/models/Blogs";
import categoriesService from "../../categories/categories.service";

async function updateBlogWithQuery(id: any, query: any) {
    await Blogs.updateOne({_id: id}, {
        $set: query
    });

    let blog = await Blogs.findById(id);
    blog.categories = await categoriesService.getCategoriesByBlogId(blog._id)

    return blog

}

export default updateBlogWithQuery;