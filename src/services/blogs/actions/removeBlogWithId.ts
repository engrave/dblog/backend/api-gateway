import { Blogs } from '../../../submodules/shared-library/models/Blogs';
import categoriesService from '../../categories/categories.service';

async function removeBlogWithId(id: string): Promise<void> {

    await Blogs.deleteOne({_id: id});

    const categories = await categoriesService.getCategoriesByBlogId(id);

    for (const category of categories) {
        await category.remove();
    }

}

export default removeBlogWithId;
