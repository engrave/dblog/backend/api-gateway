import blogsService from '../services.blogs';
import { ValidationError } from '../../../submodules/shared-library/helpers/errors/ValidationError';
import { customValidatorError } from '../../../submodules/shared-library';

async function validateBlogOwnership(blogId: string, username: string) {
    
    let blog = await blogsService.getBlogByQuery({_id: blogId});

    const isOwner = blog.owner === username;
    const isCollaborator = !!blog.collaborators.find( member => member.username === username);

    if( !isOwner && !isCollaborator ) return await customValidatorError('params', 'blogId', blogId, "You can't access this blog");
    
    return blog;
}

export default validateBlogOwnership;