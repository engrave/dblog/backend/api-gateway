import { Blogs } from "../../../submodules/shared-library/models/Blogs";
import categoriesService from "../../categories/categories.service";
import { IBlog } from "../../../submodules/shared-library/interfaces/IBlog";

async function getBlogByQuery(query: any): Promise<IBlog> {
    let blog = await Blogs.findOne(query);
    
    if(blog) {
        blog.categories = await categoriesService.getCategoriesByBlogId(blog._id);
    }

    return blog;
}

export default getBlogByQuery;