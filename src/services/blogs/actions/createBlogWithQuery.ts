import { Blogs } from "../../../submodules/shared-library/models/Blogs";

async function createBlogWithQuery(query: any) {
    
    let blog = await Blogs.create(query);
    blog.categories = new Array();
    
    return blog
}

export default createBlogWithQuery;