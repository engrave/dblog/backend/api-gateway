import { Blogs } from "../../../submodules/shared-library/models/Blogs";
import categoriesService from "../../categories/categories.service";

async function getBlogsByDomain(url: any) {
    const blogs = await Blogs.find({$or: [
        {domain: url},
        {custom_domain: url}
    ]});

    for(let blog of blogs) {
        blog.categories = await categoriesService.getCategoriesByBlogId(blog._id)
    }
    return blogs;
}

export default getBlogsByDomain;