import { Blogs } from "../../../submodules/shared-library/models/Blogs";
import categoriesService from "../../categories/categories.service";
import { IBlog } from "../../../submodules/shared-library/interfaces/IBlog";

async function getMultipleWithQuery(query: any) {
    let blogs = await Blogs.find(query);

    for(let blog of blogs) {
        blog.categories = await categoriesService.getCategoriesByBlogId(blog._id)
    }

    return blogs
}

export default getMultipleWithQuery;