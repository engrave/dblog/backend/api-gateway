import axios, { AxiosRequestConfig } from 'axios';
import { handleServiceError } from '../../../submodules/shared-library/hof/handleServiceError';

const getAccessToken = async (username: string) => {

    return handleServiceError(async () => {

        const options: AxiosRequestConfig = {
            url: `http://vault-connector:3000/access/${username}/true`,
            method: 'GET'
        };

        const { data } = await axios(options);
        return data.token;

    });

};

export default getAccessToken;
