import axios, { AxiosRequestConfig } from 'axios';
import { microservices, handleServiceError } from '../../../submodules/shared-library';

const getCoinsPrice = async (): Promise<any> => {
    return handleServiceError( async () => {
        return {
            hbd: await getPriceFromStatistics('sbd'),
            hive: await getPriceFromStatistics('steem')
        };
    });
};

const getPriceFromStatistics = async (coin: string): Promise<number> => {
    return handleServiceError( async () => {
        const { data } = await axios.get(`http://${microservices.statistics}/coins/${coin}`);
        return data.history[data.history.length - 1];
    });
};

export { getCoinsPrice };
