import axios, { AxiosRequestConfig } from 'axios';
import { microservices, handleServiceError } from '../../../submodules/shared-library';

const generateSSLCerticicate = async (domain: string) => {

    return handleServiceError( async () => {
        const options: AxiosRequestConfig = {
            url: `http://${microservices.ssl}/ssl/generate`,
            method: 'POST',
            data: { domain }
        }
    
        const { data } = await axios(options);
        return data;
    });   
}

export { generateSSLCerticicate };