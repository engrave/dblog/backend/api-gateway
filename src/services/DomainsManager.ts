import { handleServiceError } from '../submodules/shared-library/hof';
import axios, { AxiosRequestConfig } from 'axios';
import { microservices } from '../submodules/shared-library/config';
import { getCoinsPrice } from './statistics';
import { Posts } from '../submodules/shared-library/models/Posts';
import { DomainsDataService } from '../dataServices/DomainsDataService';
import { IDomain } from '../submodules/shared-library/interfaces/IDomain';

export class DomainsManager {

    public static search = async (keyword: string): Promise<any> => {
        return handleServiceError(async () => {
            const options: AxiosRequestConfig = {
                url: `http://${microservices.domains_manager}/domains/search`,
                method: 'POST',
                data: {keyword}
            };

            const {data} = await axios(options);
            const coinsPrice = await getCoinsPrice();

            return data.map((singleDomainDetails: any) => DomainsManager.transform(singleDomainDetails, coinsPrice.hive, coinsPrice.hbd));
        });
    }

    public static getDetails = async (domain: string): Promise<any> => {
        return handleServiceError(async () => {
            const options: AxiosRequestConfig = {
                url: `http://${microservices.domains_manager}/domains/details`,
                method: 'POST',
                data: {domain}
            };

            const {data} = await axios(options);
            const coinsPrice = await getCoinsPrice();

            return DomainsManager.transform(data, coinsPrice.hive, coinsPrice.hbd);
        });
    }

    public static validateDomainOwnership = async (id: string, username: string): Promise<IDomain> => {
        if (!id) {
            return null;
        }

        const domain = await DomainsDataService.getById(id);

        if (domain.owner !== username) { throw new Error('You are not the owner of that post!'); }

        return domain;
    }

    private static readonly margin: number = 1.15; // increase price by 15%

    private static transform = (details: any, hivePrice: number, hbdPrice: number): any => {
        return {
            domainName: details.domainName,
            sld: details.sld,
            tld: details.tld,
            purchasable: details.purchasable ? details.purchasable : false,
            purchaseType: details.purchaseType,
            purchasePrice: {
                hive: DomainsManager.toCoinPrice(details.purchasePrice, hivePrice),
                hbd: DomainsManager.toCoinPrice(details.purchasePrice, hbdPrice)
            },
            renewalPrice: {
                hive: DomainsManager.toCoinPrice(details.renewalPrice, hivePrice),
                hbd: DomainsManager.toCoinPrice(details.renewalPrice, hbdPrice)
            }
        };
    }

    private static toCoinPrice = (price: number, coinPrice: number): number => {
        return Math.ceil((price / coinPrice) * DomainsManager.margin); // add coin price change margin
    }
}
