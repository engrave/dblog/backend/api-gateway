declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}

global.__rootdir__ = '/app/dist';

import app from './app/app';
import { waitForMicroservice, listenOnPort, mongo, logger } from './submodules/shared-library';
import { microservices } from './submodules/shared-library/';
import * as mongoose from 'mongoose';
import blogsService from './services/blogs/services.blogs';
import { setBlog } from './submodules/shared-library/services/cache/cache';

( async () => {

    try {

        await waitForMicroservice(microservices.auth);
        await waitForMicroservice(microservices.statistics);
        await waitForMicroservice(microservices.ssl);
        await waitForMicroservice(microservices.image_uploader);
        await waitForMicroservice(microservices.sitemap_builder);
        await waitForMicroservice(microservices.domains_manager);

        await mongoose.connect(mongo.uri, mongo.options);

        try {
            const blogs = await blogsService.getMultipleWithQuery({});
            for (const blog of blogs) {
                logger.info(`Refreshing ${blog.domain}`);
                await setBlog(blog);
            }
        } catch (error) {
            logger.error(error);
        }

        listenOnPort(app, 3000);

    } catch (error) {
        logger.warn('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }

})();
