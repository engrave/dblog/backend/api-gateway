import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { param } from 'express-validator/check';

import blogsService from '../../../services/blogs/services.blogs';
import { blogExists } from '../../../validators/blog/blogExiststs';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';

const middleware: any[] =  [
    param('id').isMongoId().custom(blogExists).withMessage('Blog does not exist')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        
        const { id } = req.params;
        const { username } = res.locals;
    
        const blog = await blogsService.getBlogByQuery({_id: id});

        await validateBlogOwnership(id, username);

        return res.json(blog);

    }, req, res);
}

export default {
    middleware,
    handler
}