import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { param } from 'express-validator/check';

import blogsService from '../../../services/blogs/services.blogs';
import { removeByBlogId } from '../../../dataServices/posts';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { removeBlog } from '../../../submodules/shared-library/services/cache/cache';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import {removeNginxConfiguration} from "../../../services/nginx-configurator/actions/removeNginxConfiguration";

const middleware: any[] = [
    param('id').isString().custom(blogExists).withMessage('Blog does not exist')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { id } = req.params;
        const { username } = res.locals;

        const blog = await blogsService.getBlogByQuery({ _id: id });

        await validateBlogOwnership(id, username);

        await removeBlog(blog);

        await blogsService.removeBlogWithId(id);
        await removeByBlogId(id);

        if(blog.custom_domain) {
            await removeNginxConfiguration(blog.custom_domain);
        }

        return res.json({
            success: 'OK'
        });

    }, req, res);
}

export default {
    middleware,
    handler
}
