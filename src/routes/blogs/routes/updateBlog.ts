import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, logger, steem} from '../../../submodules/shared-library';
import { body, param } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import isDomainValid from '../../../validators/domain/isDomainValid';
import blogsService from '../../../services/blogs/services.blogs';
import { validateAddressIsFree } from '../../../validators/url/validateAddressIsFree';
import { isValidSubdomain } from '../../../validators/url/isValidSubdomain';
import { setBlog, getBlog, removeBlog } from '../../../submodules/shared-library/services/cache/cache';
import rebuildSitemap from '../../../services/sitemap/actions/rebuildSitemap';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import { generateSSLCerticicate } from '../../../services/ssl-manager';
import { generateNginxConfiguration } from '../../../services/nginx-configurator';
import {removeNginxConfiguration} from "../../../services/nginx-configurator/actions/removeNginxConfiguration";

const middleware: any[] = [
    param('id').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    body('domain').optional()
        .isString().not().isEmpty()
        .isURL().withMessage('Please provide valid subdomain address')
        .custom(isValidSubdomain).withMessage('This is not a proper subdomain')
        .trim(),
    body('custom_domain').optional({checkFalsy: true})
        .isString().not().isEmpty()
        .isURL()
        .custom(isDomainValid).withMessage('Domain is not pointing to Engrave server. Validate your DNS settings')
        .custom(async (domain: string) => await isDomainValid(`www.${domain}`)).withMessage('Domain is not pointing to Engrave server. Validate your DNS settings')
        .trim(),
    body('domain_redirect').optional().isBoolean(),
    body('title').optional().isString().trim(),
    body('slogan').optional().isString().trim(),
    body('logo_url').optional().isString().isURL(),
    body('main_image').optional({checkFalsy: true}).isString().isURL(),

    body('link_facebook').optional({checkFalsy: true}).isString().isURL(),
    body('link_twitter').optional({checkFalsy: true}).isString().isURL(),
    body('link_linkedin').optional({checkFalsy: true}).isString().isURL(),
    body('link_instagram').optional({checkFalsy: true}).isString().isURL(),

    body('opengraph_default_image_url').optional({checkFalsy: true}).isString().isURL().withMessage('Please provide valid OpenGraph image URL'),
    body('opengraph_default_description').optional({checkFalsy: true}).isString(),
    body('onesignal_app_id').optional({checkFalsy: true}).isString(),
    body('onesignal_api_key').optional({checkFalsy: true}).isString(),
    body('onesignal_body_length').optional({checkFalsy: true}).isNumeric(),
    body('onesignal_logo_url').optional({checkFalsy: true}).isString().isURL(),
    body('analytics_gtag').optional({checkFalsy: true}).isString(),
    body('webmastertools_id').optional({checkFalsy: true}).isString(),
    body('webmastertools_id').optional({checkFalsy: true}).isString(),
    body('hidden_commenters.*').optional({checkFalsy: true}).matches(steem.usernameRegex).withMessage('It is not a valid Steem username'),
    body('theme').optional({checkFalsy: true}).isString().trim(), // TODO validate if a theme exists

    // prohibited
    body('collaboration_type').not().exists().withMessage('You tried to become a hacker, don\'t you ? '),
    body('premium').not().exists().withMessage('You tried to become a hacker, don\'t you ? '),
    body('owner').not().exists().withMessage('You tried to become a hacker, don\'t you ? '),
    body('_id').not().exists().withMessage('You tried to become a hacker, don\'t you?'),
    body('categories').not().exists().withMessage('To update categories, use another endpoint'),
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {

        const { id } = req.params;
        const { domain, custom_domain } = req.body;
        const { username } = res.locals;

        await validateBlogOwnership(id, username);

        if (domain) {
            await validateAddressIsFree(domain, id);
        }

        if (custom_domain) {
            try {
                await validateAddressIsFree(custom_domain, id);
                await generateNginxConfiguration(custom_domain);
                await generateSSLCerticicate(custom_domain);
                await generateNginxConfiguration(custom_domain);
            } catch (e) {
                logger.error(e, 'Error while generating SSL certificate');
                await removeNginxConfiguration(custom_domain);
                throw e;
            }
        }

        const oldBlog = await blogsService.getBlogByQuery({_id: id});
        const blog = await blogsService.updateBlogWithQuery(id, req.body);

        if (oldBlog.custom_domain && oldBlog.custom_domain !== custom_domain) {
            await removeNginxConfiguration(oldBlog.custom_domain);
        }

        if (oldBlog.domain !== blog.domain || oldBlog.custom_domain !== blog.custom_domain) {
            await removeBlog(oldBlog);
        }

        await setBlog(blog);

        await rebuildSitemap(blog);

        return res.json({ status: 'OK', blog });

    }, req, res);
}

export default {
    middleware,
    handler
};
