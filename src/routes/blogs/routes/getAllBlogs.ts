import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import blogsService from '../../../services/blogs/services.blogs';
import { IBlog } from '../../../submodules/shared-library/interfaces/IBlog';

const middleware: any[] =  [
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        
        const { username } = res.locals;
        
        const blogs: IBlog[] = await blogsService.getMultipleWithQuery({$or: [{owner: username}, {'collaborators.username': username}] });
        
        return res.json([ ...blogs ]);

    }, req, res);
}

export default {
    middleware,
    handler
}