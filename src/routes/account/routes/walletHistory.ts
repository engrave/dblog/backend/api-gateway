import {Request, RequestHandler, Response} from 'express';
import { handleResponseError, logger, microservices, steem } from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';
import { param } from 'express-validator/check';

const middleware: RequestHandler[] =  [
    param('username').isString().matches(steem.usernameRegex).withMessage('Invalid Hive username')
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {

        const options: AxiosRequestConfig = {
            method: 'GET',
            url: `http://${microservices.statistics}/wallet/${req.params.username}/history`
        };

        const {data} = await axios(options);

        return res.json(data);

    }, req, res);
}

export default {
    middleware,
    handler
};
