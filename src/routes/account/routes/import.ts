import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { importPosts } from '../../../controllers/account';
import { body } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { categoryExist } from '../../../validators/categories/categoryExist';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import validateCategories from '../../../services/categories/actions/validateCategories';

const middleware: any[] =  [
    body('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    body('categoryId').isMongoId().custom(categoryExist).withMessage('Category does not exist')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId, categoryId } = req.body;

        await validateBlogOwnership(blogId, username);
        await validateCategories([categoryId], blogId);

        return importPosts(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}
