import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import { modifyAccount } from '../../../controllers/account';

const middleware: any[] =  [
    body('about').optional().isString(),
    body('name').optional().isString(),
    body('location').optional().isString(),
    body('website').optional().isURL(),
    body('profile_image').optional().isURL(),
    body('cover_image').optional().isURL()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        return await modifyAccount(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}