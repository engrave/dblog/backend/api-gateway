import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { getStats } from '../../../controllers/account';
import { param } from 'express-validator/check';
const middleware: any[] =  [
    param('username').isString().matches(steem.usernameRegex).withMessage("Invalid Hive username")
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        return await getStats(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}
