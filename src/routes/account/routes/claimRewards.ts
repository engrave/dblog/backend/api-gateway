import {Request, RequestHandler, Response} from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import { claimAllRewards } from '../../../controllers/account';

const middleware: RequestHandler[] =  [
    body('username').isString().matches(steem.usernameRegex).withMessage('Invalid Hive username')
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {

        return claimAllRewards(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
};
