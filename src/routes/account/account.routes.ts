import * as express from 'express';
import verifyToken from '../../middlewares/jwt/verifyToken';
import importPosts from './routes/import';
import claimRewards from './routes/claimRewards';
import modify from './routes/modify';
import stats from './routes/stats';
import walletHistory from './routes/walletHistory';

import {endpointLogger} from '../../submodules/shared-library/utils/logger';

const accountApi: express.Router = express.Router();

accountApi.put('/', verifyToken, modify.middleware, modify.handler);
accountApi.put('/import', endpointLogger, verifyToken, importPosts.middleware, importPosts.handler);
accountApi.post('/rewards/claim', endpointLogger, verifyToken, claimRewards.middleware, claimRewards.handler);
accountApi.get('/:username/rc', stats.middleware, stats.handler);
accountApi.get('/:username/wallet/history', walletHistory.middleware, walletHistory.handler);

export default accountApi;
