import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, microservices} from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';

const middleware: RequestHandler[] =  [
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {

        const options: AxiosRequestConfig = {
            method: 'GET',
            url: `http://${microservices.statistics}/coins/btc`
        };

        const {data} = await axios(options);

        return res.json(data);

    }, req, res);
}

export default {
    middleware,
    handler
};