import * as express from 'express';
import hbd from './routes/hbd';
import hive from './routes/hive';
import btc from './routes/btc';

const coinsApi: express.Router = express.Router();

coinsApi.get('/hbd', hbd.middleware, hbd.handler);
coinsApi.get('/sbd', hbd.middleware, hbd.handler);
coinsApi.get('/hive', hive.middleware, hive.handler);
coinsApi.get('/steem', hive.middleware, hive.handler);
coinsApi.get('/btc', btc.middleware, btc.handler);

export default coinsApi;
