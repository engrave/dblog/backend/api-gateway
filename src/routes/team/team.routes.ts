import * as express from "express";
import getMembers from "./routes/getMembers";
import addMember from "./routes/addMember";
import verifyToken from "../../middlewares/jwt/verifyToken";
import removeMember from "./routes/removeMember";

const teamApi: express.Router = express.Router();

teamApi.get('/:blogId/team', verifyToken, getMembers.middleware, getMembers.handler);
teamApi.post('/:blogId/team', verifyToken, addMember.middleware, addMember.handler);
teamApi.delete('/:blogId/team', verifyToken, removeMember.middleware, removeMember.handler);

export default teamApi;