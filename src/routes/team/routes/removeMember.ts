import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { param, body } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { removeMember } from '../../../controllers/team';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import { validateIsCollaborator } from '../../../validators/team/validateIsCollaborator';

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    body("username").isString().matches(steem.usernameRegex).withMessage('It is not a valid Steem username')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        
        const { username } = res.locals;
        const { blogId } = req.params;

        await validateBlogOwnership(blogId, username);
        await validateIsCollaborator(blogId, req.body.username);

        return await removeMember(req, res);
        
    }, req, res);
}

export default {
    middleware,
    handler
}