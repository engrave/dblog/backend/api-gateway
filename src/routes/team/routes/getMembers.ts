import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { param } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { getMembers } from '../../../controllers/team';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
                
        const { username } = res.locals;
        const { blogId } = req.params;

        await validateBlogOwnership(blogId, username);
        
        return await getMembers(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}