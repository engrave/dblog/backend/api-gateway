import * as express from 'express';
import reserveDomain from './routes/reserveDomain';
import getDetails from './routes/getDetails';
import searchDomains from './routes/searchDomains';
import listDomains from './routes/listDomains';
import cancelReservation from './routes/cancelReservation';

import verifyToken from '../../middlewares/jwt/verifyToken';
const domainsApi: express.Router = express.Router();

domainsApi.get('/', verifyToken, listDomains.middleware, listDomains.handler);
domainsApi.post('/details', getDetails.middleware, getDetails.handler);
domainsApi.post('/search', searchDomains.middleware, searchDomains.handler);
domainsApi.post('/reserve', verifyToken, reserveDomain.middleware, reserveDomain.handler);
domainsApi.delete('/:id', verifyToken, cancelReservation.middleware, cancelReservation.handler);

export default domainsApi;
