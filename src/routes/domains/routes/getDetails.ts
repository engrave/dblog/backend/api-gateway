import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, microservices} from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';
import { getDomainDetails } from '../../../controllers/domains';
import { body } from 'express-validator/check';

const middleware: RequestHandler[] =  [
    body('domain').exists().trim().stripLow().customSanitizer((v) => v.toLowerCase()).isString().isURL().withMessage('Invalid domain name')
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {
        return getDomainDetails(req, res);
    }, req, res);
}

export default {
    middleware,
    handler
};
