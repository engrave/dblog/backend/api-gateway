import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, microservices} from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';
import { getDomainDetails } from '../../../controllers/domains';
import { reserveDomain } from '../../../controllers/domains/actions/reserveDomain';
import { body } from 'express-validator/check';
import { isDomainFree } from '../../../validators/domains/isDomainFree';

const middleware: RequestHandler[] =  [
    body('domain').exists().trim().stripLow().customSanitizer((v) => v.toLowerCase()).isString()
        .isURL().withMessage('Invalid domain name')
        .custom(isDomainFree).withMessage('Domain already taken')
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {
        return reserveDomain(req, res);
    }, req, res);
}

export default {
    middleware,
    handler
};
