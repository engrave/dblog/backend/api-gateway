import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, microservices} from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';
import { listDomains } from '../../../controllers/domains/actions/listDomains';

const middleware: RequestHandler[] =  [

];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {
        return listDomains(req, res);
    }, req, res);
}

export default {
    middleware,
    handler
};
