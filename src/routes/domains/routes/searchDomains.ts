import {Request, RequestHandler, Response} from 'express';
import {handleResponseError} from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import { searchDomains } from '../../../controllers/domains/actions/searchDomains';

const middleware: RequestHandler[] =  [
    body('keyword').exists().trim().stripLow().customSanitizer((v) => v.toLowerCase()).isString()
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {
        return searchDomains(req, res);
    }, req, res);
}

export default {
    middleware,
    handler
};
