import {Request, RequestHandler, Response} from 'express';
import {handleResponseError, microservices} from '../../../submodules/shared-library';
import axios, { AxiosRequestConfig } from 'axios';
import { cancelReservation } from '../../../controllers/domains/actions/cancelReservation';
import { DomainsManager } from '../../../services/DomainsManager';
import { param } from 'express-validator/check';
import { domainReservationExists } from '../../../validators/domains/domainReservationExists';

const middleware: RequestHandler[] =  [
    param('id').exists().isMongoId().custom(domainReservationExists).withMessage('Invalid reservation ID')
];

async function handler(req: Request, res: Response): Promise<RequestHandler> {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { id } = req.params;

        await DomainsManager.validateDomainOwnership(id, username);

        return cancelReservation(req, res);
    }, req, res);
}

export default {
    middleware,
    handler
};
