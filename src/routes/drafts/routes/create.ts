import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import { createDraft } from '../../../controllers/drafts';
import { blogExists } from '../../../validators/blog/blogExiststs';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import validateCategories from '../../../services/categories/actions/validateCategories';

const middleware: any[] =  [
    body('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    body('title').isString().withMessage('Title cannot be empty'),
    body('body').isString().withMessage('Article body cannot be empty'),
    body('permlink').isString().withMessage('Article permlink cannot be empty'),

    body('featured_image').optional().isURL().withMessage('Invalid featured image URL'),
    body('decline_reward').optional().isBoolean().toBoolean(),

    body('scheduled').optional().isString(), // isDate
    body('categories').optional().isArray().withMessage('Categories need to be an array'),
    body('categories.*').optional().isMongoId().withMessage('Should be category ID'),
    body('tags').optional().isArray().withMessage('Tags need to be an array').custom((tags) => (tags.length <= 5)).withMessage('Use no more than 5 tags'),
    body('tags.*').optional().matches(steem.tagRegex).withMessage('Invalid Hive tag')

];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId } = req.params;

        const { categories } = req.body;

        await validateBlogOwnership(blogId, username);
        await validateCategories(categories, blogId);

        return createDraft(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
};
