import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { param } from 'express-validator/check';
import { draftExists } from '../../../validators/drafts/draftExists';
import validatePostOwnership from '../../../services/posts/actions/validatePostOwnership';
import { removeDraft } from '../../../controllers/drafts';

const middleware: any[] =  [
    param('id').isMongoId().custom(draftExists).withMessage("This draft does not exist")
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        
        const { id } = req.params;
        const { username } = res.locals;

        await validatePostOwnership(id, username);

        return await removeDraft(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}