import * as express from "express";
import get from "./routes/get";
import getAll from "./routes/getAll";
import create from './routes/create';
import remove from './routes/remove';
import update from './routes/update';
import verifyToken from "../../middlewares/jwt/verifyToken";

const draftsApi: express.Router = express.Router();

draftsApi.put('/:blogId/drafts/:id', verifyToken, update.middleware, update.handler);
draftsApi.post('/:blogId/drafts', verifyToken, create.middleware, create.handler);
draftsApi.get('/:blogId/drafts', verifyToken, getAll.middleware, getAll.handler);
draftsApi.get('/:blogId/drafts/:id', verifyToken, get.middleware, get.handler);
draftsApi.delete('/:blogId/drafts/:id', verifyToken, remove.middleware, remove.handler);

export default draftsApi;