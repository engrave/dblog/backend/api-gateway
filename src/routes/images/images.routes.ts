import * as express from "express";
import upload from "./routes/upload";
import verifyToken from "../../middlewares/jwt/verifyToken";

const multer = require('multer');
const ignoreMulterErrors = multer({ storage: multer.memoryStorage() }).single('file');

const postsApi: express.Router = express.Router();

postsApi.post('/', verifyToken, (req: any, res: any, next: any) => ignoreMulterErrors(req, res, (err: any) => next()), upload.middleware, upload.handler);

export default postsApi;