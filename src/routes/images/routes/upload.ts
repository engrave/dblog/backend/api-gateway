import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { uploadImage } from '../../../controllers/images';
import { body } from 'express-validator/check';

const middleware: any[] =  [
    body('url').optional().isString().isURL(),
    body('file').optional()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        return await uploadImage(req, res);
        
    }, req, res);
}

export default {
    middleware,
    handler
}