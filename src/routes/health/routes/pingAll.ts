import { Request, Response } from 'express';
import { handleResponseError, generateHealthResponse, microservices } from '../../../submodules/shared-library';
import axios from 'axios';

const middleware: any[] = [];

async function handler(req: Request, res: Response): Promise<any>{
    return handleResponseError(async () => {
        const responses: { [index: string]: {} } = {};

        for (const service in microservices) {
            responses[service] = await getMicroserviceHealthResponse(microservices[service]);
        }
        return res.json(responses);
    }, req, res);
}

const getMicroserviceHealthResponse = async (microservice: string) => {
    try {
        const requestStartTime = new Date().getTime();

        const {data} = await axios.get(`http://${microservice}/health/ping`);

        return {
            ...data,
            response_delay: new Date().getTime() - requestStartTime
        };
    } catch (error) {
        return 'unhealthy';
    }
};

export default {
    middleware,
    handler
};