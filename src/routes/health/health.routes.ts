import * as express from "express";
import ping from "./routes/ping";
import pingAll from "./routes/pingAll";

const healthApi: express.Router = express.Router();

healthApi.get('/ping', ping.middleware, ping.handler);
healthApi.get('/ping/all', pingAll.middleware, pingAll.handler);

export default healthApi;