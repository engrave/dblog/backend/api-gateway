import * as express from "express";
import verifyToken from "../../middlewares/jwt/verifyToken";
import publish from "./routes/publish";
import getPublished from "./routes/getPublished";
import removePost from "./routes/removePost";
import hidePost from "./routes/hidePost";
import unhidePost from "./routes/unhidePost";
import editPublished from "./routes/editPublished";
import migratePosts from "./routes/migratePosts";
import getSinglePublished from "./routes/getSinglePublished";
import moveToBlog from './routes/moveToBlog';

const postsApi: express.Router = express.Router();

postsApi.post('/:blogId/posts', verifyToken, publish.middleware, publish.handler);
postsApi.get('/:blogId/posts', verifyToken, getPublished.middleware, getPublished.handler);
postsApi.get('/:blogId/posts/:postId', verifyToken, getSinglePublished.middleware, getSinglePublished.handler);
postsApi.delete('/:blogId/posts/:postId', verifyToken, removePost.middleware, removePost.handler);
postsApi.put('/:blogId/posts/:postId/hide', verifyToken, hidePost.middleware, hidePost.handler);
postsApi.put('/:blogId/posts/:postId/unhide', verifyToken, unhidePost.middleware, unhidePost.handler);
postsApi.put('/:blogId/posts/:postId/move', verifyToken, moveToBlog.middleware, moveToBlog.handler);
postsApi.put('/:blogId/posts/:postId', verifyToken, editPublished.middleware, editPublished.handler);
postsApi.put('/:blogId/migrate', migratePosts.middleware, migratePosts.handler);

export default postsApi;
