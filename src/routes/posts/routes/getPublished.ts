import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { param, query } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import { getPublished } from '../../../controllers/posts';

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    query('page').optional().isNumeric().toInt(),
    query('limit').optional().isNumeric().toInt(),
    query('sortBy').optional().isString(),
    query('order').optional().isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId } = req.params;

        await validateBlogOwnership(blogId, username);

        return await getPublished(req, res)

    }, req, res);
}

export default {
    middleware,
    handler
}