import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body, param } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { categoryExist } from '../../../validators/categories/categoryExist';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import validatePostOwnership from '../../../services/posts/actions/validatePostOwnership';
import validateCategories from '../../../services/categories/actions/validateCategories';
import { moveToAnotherBlog } from '../../../controllers/posts/actions/moveToAnotherBlog';

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    param('postId').isMongoId().withMessage('Article does not exist'),

    body('blogId').isMongoId().custom(blogExists).withMessage('Destination blog does not exist'),
    body('categoryId').isMongoId().custom(categoryExist).withMessage('Destination category does not exist'),
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId, postId } = req.params;
        const { blogId: destBlogId, categoryId: destCategoryId } = req.body;

        await validateBlogOwnership(blogId, username);
        await validatePostOwnership(postId, username);

        await validateBlogOwnership(destBlogId, username);
        await validateCategories([destCategoryId], destBlogId);

        return moveToAnotherBlog(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}
