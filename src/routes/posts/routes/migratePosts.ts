import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import { migratePosts } from '../../../controllers/posts';

const middleware: any[] = [
    body('posts.*.username').isString().not().isEmpty(),
    body('posts.*.permlink').isString(),
    body('posts.*.category').isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        return await migratePosts(req, res)

    }, req, res);
}

export default {
    middleware,
    handler
}

