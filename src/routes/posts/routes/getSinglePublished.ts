import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { param, query } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import validatePostOwnership from '../../../services/posts/actions/validatePostOwnership';
import { getSinglePublished } from '../../../controllers/posts';

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    param('postId').isMongoId().withMessage('Article does not exist')
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId, postId } = req.params;

        await validateBlogOwnership(blogId, username);
        await validatePostOwnership(postId, username);

        return await getSinglePublished(req, res)

    }, req, res);
}

export default {
    middleware,
    handler
}