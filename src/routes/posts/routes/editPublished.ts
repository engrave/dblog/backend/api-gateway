import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body, param } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import { draftExists } from '../../../validators/drafts/draftExists';
import validatePostOwnership from '../../../services/posts/actions/validatePostOwnership';
import validateCategories from '../../../services/categories/actions/validateCategories';
import { editPublishedPost } from '../../../controllers/posts'

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),
    param('postId').isMongoId().withMessage('Article does not exist'),
    
    body("permlink").optional().isString().isLength({min: 2, max: 160}),
    body('title').optional().isString(),
    body('body').optional().isString(),
    body('thumbnail').optional().isURL(),
    body('categories').optional().isArray().withMessage("Categories need to be an array"),
    body('categories.*').optional().isMongoId().withMessage("Should be category ID"),
    body('tags').optional().isArray().withMessage("Tags need to be an array").custom(tags => (tags.length <= 5)).withMessage("Use no more than 5 tags"),
    body('tags.*').optional().matches(steem.tagRegex).withMessage("Invalid Hive tag"),
    
    body('draftId').optional().isMongoId().custom(draftExists).withMessage('Draft does not exist'),
    
    // prohibited
    body('username').not().exists().withMessage("Bad boy! You cannot change username"),
    body('status').not().exists().withMessage("Bad boy! You cannot change draft status"),
    body('_id').not().exists().withMessage("Bad boy! You tried to become a hacker, don\'t you?"),
    body('blogId').not().exists().withMessage("Bad boy! You tried to become a hacker, don\'t you?")
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId, postId } = req.params;
        const { categories } = req.body;

        await validateBlogOwnership(blogId, username);
        await validateCategories(categories, blogId);
        await validatePostOwnership(postId, username);
 
       return await editPublishedPost(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
}
