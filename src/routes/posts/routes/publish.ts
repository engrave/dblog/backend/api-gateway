import { Request, Response } from 'express';
import { handleResponseError, steem } from '../../../submodules/shared-library';
import { body, param } from 'express-validator/check';
import { blogExists } from '../../../validators/blog/blogExiststs';
import { draftExists } from '../../../validators/drafts/draftExists';
import { publishPost } from '../../../controllers/posts';
import validateBlogOwnership from '../../../services/blogs/actions/validateBlogOwnership';
import validateCategories from '../../../services/categories/actions/validateCategories';
import validatePostOwnership from '../../../services/posts/actions/validatePostOwnership';
import { articleExists } from '../../../validators/hive/articleExists';
import validatePermlinkIsNotTaken from '../../../services/posts/actions/validatePermlinkIsNotTaken';

const STEEM_TAGS_MAX_QUANTITY = 15;

const middleware: any[] =  [
    param('blogId').isMongoId().custom(blogExists).withMessage('Blog does not exist'),

    body('permlink').isString().isLength({min: 2, max: 160}),
    body('title').isString(),
    body('body').isString(),
    body('decline_reward').isBoolean().toBoolean(),
    body('thumbnail').optional().isURL(),
    body('categories').optional().isArray().withMessage('Categories need to be an array'),
    body('categories.*').optional().isMongoId().withMessage('Should be category ID'),
    body('tags').optional().isArray().withMessage('Tags need to be an array').custom(tags => (tags.length <= STEEM_TAGS_MAX_QUANTITY)).withMessage('Use no more than 5 tags'),
    body('tags.*').optional().matches(steem.tagRegex).withMessage('Invalid Hive tag'),

    body('draftId').optional().isMongoId().custom(draftExists).withMessage('Draft does not exist'),
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { username } = res.locals;
        const { blogId } = req.params;
        const { draftId, categories, permlink } = req.body;

        await validateBlogOwnership(blogId, username);
        await validateCategories(categories, blogId);
        await validatePostOwnership(draftId, username);
        await validatePermlinkIsNotTaken(blogId, permlink);

        if (await articleExists(username, permlink)) {
            throw new Error('Article with that permlink already exists');
        }

        return publishPost(req, res);

    }, req, res);
}

export default {
    middleware,
    handler
};
