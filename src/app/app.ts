import * as express from 'express';
import routes from './app.routes';
import settings from './app.settings';
const Sentry = require('@sentry/node');
import { RewriteFrames, Debug } from '@sentry/integrations';

Sentry.init({
    dsn: 'https://b3e0c2ae15fa41b7a545defc2c7f89d3@sentry.engrave.dev/3',
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    integrations: [
        new RewriteFrames({ root: global.__rootdir__ })
    ]});

const app = express();

settings(app);

routes(app);

export default app;
