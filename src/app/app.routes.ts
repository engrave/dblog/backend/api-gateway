import healthApi from '../routes/health/health.routes';
import postsApi from '../routes/posts/posts.routes';
import blogsApi from '../routes/blogs/blogs.routes';
import coinsApi from '../routes/coins/coins.routes';
import draftsApi from '../routes/drafts/drafts.routes';
import categoriesApi from '../routes/categories/categories.routes';
import exploreApi from '../routes/explore/explore.routes';
import teamApi from '../routes/team/team.routes';
import imagesApi from '../routes/images/images.routes';
import accountApi from '../routes/account/account.routes';
import domainsApi from '../routes/domains/domains.routes';

import * as httpCodes from 'http-status-codes';
import { Request, Response } from 'express';
import {endpointLogger} from '../submodules/shared-library/utils/logger';

const Sentry = require('@sentry/node');

function routes(app: any): void {
    app.use('/health', healthApi);
    app.use('/explore', exploreApi);

    app.use('/account', accountApi);
    app.use('/blogs', endpointLogger, blogsApi);
    app.use('/blogs', endpointLogger, teamApi);
    app.use('/blogs', endpointLogger, draftsApi);
    app.use('/blogs', endpointLogger, postsApi);
    app.use('/categories', endpointLogger, categoriesApi);
    app.use('/coins', coinsApi);
    app.use('/images', endpointLogger, imagesApi);
    app.use('/domains', endpointLogger, domainsApi);

    app.get('/debug-sentry', (req: Request, res: Response) => {
        throw new Error('My first Sentry error!');
    });

    app.use('*', (req: Request, res: Response) => {res.status(httpCodes.NOT_FOUND).json({message: 'Resource not found'}); });

    // The error handler must be before any other error middleware and after all controllers
    app.use(Sentry.Handlers.errorHandler());
}

export default routes;
