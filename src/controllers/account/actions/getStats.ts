import { Request, Response } from 'express';
import Axios, { AxiosRequestConfig } from 'axios';
import { blockchain } from '../../../submodules/shared-library/config';

const getStats = async (req: Request, res: Response) => {

    const { username } = req.params;

    const request = await getAccountRc(username);

    return res.json({ status: 'OK', result: request.result.rc_accounts[0]});

};

const getAccountRc = async (username: string) => {
    const options: AxiosRequestConfig = {
        method: 'POST',
        url: blockchain.node,
        data: {
            jsonrpc: '2.0',
            id: 1,
            method: 'rc_api.find_rc_accounts',
            params: {
                accounts: [username]
            }
        }
    };

    const { data } = await Axios.request(options);
    return data;
};

export { getStats };
