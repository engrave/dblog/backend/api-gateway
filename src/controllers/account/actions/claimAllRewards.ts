import { Request, Response } from 'express';
import sc from '../../../submodules/shared-library/services/steemconnect/hivesigner.service';
import getAccessToken from '../../../services/vault/actions/getAccessToken';

const claimAllRewards = async (req: Request, res: Response): Promise<any> => {

    const { username } = res.locals;

    // tslint:disable-next-line:variable-name
    const access_token = await getAccessToken(username);

    if (!access_token) {
        throw new Error('Could not authorize user (vault is sealed)');
    }

    const result = await sc.claimRewards(access_token, username);

    return res.json({ status: 'OK' });

};

export { claimAllRewards };
