import {Request, Response} from 'express';
import {createWithQuery, getSingle} from '../../../dataServices/posts';
import {PostStatus, hive, logger} from '../../../submodules/shared-library';
import getCategoryById from '../../../services/categories/actions/getCategoryById';
import {addArticle} from '../../../submodules/shared-library/services/cache/cache';
import {updateByUsername} from '../../../dataServices/users';
import rebuildSitemap from '../../../services/sitemap/actions/rebuildSitemap';
import blogsService from '../../../services/blogs/services.blogs';

const importPosts = async (req: Request, res: Response): Promise<Response> => {

    const {username} = res.locals;
    const {blogId, categoryId} = req.body;

    const blog = await blogsService.getBlogByQuery({_id: blogId});
    const category = await getCategoryById(categoryId);

    logger.info(`Importing articles for ${username} from blockchain`, {blogId, categoryId, username});

    const limit = 20;
    let total = 0;
    let count = limit;
    // tslint:disable-next-line
    let start_permlink = null;
    // tslint:disable-next-line
    let start_author = null;

    while (count >= limit) {

        const articles: any[] = await hive.call('bridge', 'get_account_posts', {
            account: username,
            sort: 'posts',
            limit,
            start_author,
            start_permlink
        });

        for (const article of articles) {
            if (await shouldBeImported(article, username)) {
                try {
                    logger.info(`Importing article ${article.permlink} by ${article.author}`);
                    await createWithQuery(preparePostFromBlockchain(article, blogId, categoryId, username));
                    await addArticle(blogId, article.permlink, article, [category]);
                    total += 1;
                } catch (error) {
                    logger.error(error);
                    logger.error(`Error importing article ${article.permlink} by ${article.author}`, {error, article});
                }
            } else {
                logger.info(`Skipping article ${article.permlink} by ${article.author}`);
            }
        }

        count = articles.length;

        if (articles.length === limit) {
            start_permlink = articles[articles.length - 1].permlink;
            start_author = articles[articles.length - 1].author;
        }
    }

    // NOTE it's useless as we don't validate this field anymore. It is just informative.
    await updateByUsername(username, {$set: {imported: true}});
    await rebuildSitemap(blog);

    return res.json({status: 'OK', total});

};

const preparePostFromBlockchain = (hiveArticle: any, blogId: any, categoryId: string, username: string): any => {

    const metadata = typeof hiveArticle.json_metadata === 'string' ? JSON.parse(hiveArticle.json_metadata) : hiveArticle.json_metadata;

    return {
        blogId,
        username,
        author: username,
        scheduledAt: null,
        title: hiveArticle.title,
        body: hiveArticle.body,
        categories: [categoryId],
        tags: prepareTags(hiveArticle),
        featured_image: metadata.image ? metadata.image[0] ? metadata.image[0] : null : null,
        thumbnail_image: metadata.image ? metadata.image[0] ? metadata.image[0] : null : null,
        status: PostStatus.PUBLISHED,
        decline_reward: hiveArticle.max_accepted_payout === '0.000 HBD' ? true : false,
        permlink: hiveArticle.permlink,
        parent_category: hiveArticle.category,
        publishedAt: hiveArticle.created,
        createdAt: hiveArticle.created,
        updatedAt: hiveArticle.created
    };
};

const prepareTags = (hiveArticle: any): string[] => {

    const {tags} = typeof hiveArticle.json_metadata === 'string' ? JSON.parse(hiveArticle.json_metadata) : hiveArticle.json_metadata;

    let engraveTags = [hiveArticle.category];

    if (tags instanceof Array) {
        engraveTags = [...engraveTags, ...tags];
    } else if (typeof (tags) === 'string' || tags instanceof String) {
        engraveTags = [...engraveTags, ...tags.split(' ')];
    }
    return [...new Set(engraveTags)];
};

const shouldBeImported = async (article: any, username: string): Promise<boolean> => {
    if (article.author === username && !await getSingle({username, permlink: article.permlink})) {
        return true;
    }

    return false;
};

export {importPosts, preparePostFromBlockchain, shouldBeImported};
