import { Request, Response } from 'express';
import sc from '../../../submodules/shared-library/services/steemconnect/hivesigner.service';
import getAccessToken from '../../../services/vault/actions/getAccessToken';
import { handleHivesignerError } from '../../../submodules/shared-library';

const modifyAccount = async (req: Request, res: Response) => {

    const { username } = res.locals;

    console.log(username);

    const access_token = await getAccessToken(username);

    if(!access_token) {
        throw new Error("Could not authorize user (vault is sealed)");
    }

    let operations: any[] = [
        ['account_update',
            {
                account: username,
                json_metadata: JSON.stringify(prepareAccountMetadata(req))
            }
        ]
    ];

    await handleHivesignerError( async () => {
        sc.dashboard.setAccessToken(access_token);
        await sc.dashboard.broadcast(operations);
    })


    return res.json({ status: "OK" });

};

const prepareAccountMetadata = (data: any) => {
    const profile = {
        name: data.name ? data.name : null,
        about: data.about ? data.about: null,
        location: data.location ? data.location: null,
        website: data.website ? data.website: null,
        profile_image: data.profile_image ? data.profile_image: null,
        cover_image: data.cover_image ? data.cover_image: null
    }
    return { profile };
}

export { modifyAccount };
