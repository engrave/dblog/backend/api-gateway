import { Request, Response } from 'express';
import { getSingle } from '../../../dataServices/posts';

const getSinglePublished = async (req: Request, res: Response) => {

    const { postId } = req.params;

    const query = { _id: postId }; // TODO check if it shouldn't be status draft

    const result = await getSingle(query);

    return res.json(result);

};

export { getSinglePublished };