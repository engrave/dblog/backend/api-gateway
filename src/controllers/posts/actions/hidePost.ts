import { Request, Response } from 'express';
import { getSingle } from '../../../dataServices/posts';
import { removeArticle } from '../../../submodules/shared-library/services/cache/cache';

const hidePost = async (req: Request, res: Response) => {

    const { blogId, postId } = req.params;

    const article = await getSingle({_id: postId});

    await removeArticle(blogId, article.permlink);

    article.hidden = true;
    await article.save();

    return res.json({ status: "OK", post: article});

};

export { hidePost };