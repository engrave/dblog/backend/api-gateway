import { Request, Response } from 'express';
import { getSingle } from '../../../dataServices/posts';
import moveToBlog from '../../../submodules/shared-library/services/cache/actions/articles/moveToBlog';
import categoriesService from '../../../services/categories/categories.service';

const moveToAnotherBlog = async (req: Request, res: Response) => {

    const { blogId, postId } = req.params;
    const { blogId: destBlogId, categoryId: destCategoryId } = req.body;

    const article = await getSingle({_id: postId});
    const category = await categoriesService.getCategoryById(destCategoryId);

    await moveToBlog(blogId, destBlogId, category, article.permlink);

    article.blogId = destBlogId;
    article.categories = [destCategoryId];

    await article.save();

    return res.json({ status: 'OK', post: article});

};

export { moveToAnotherBlog };
