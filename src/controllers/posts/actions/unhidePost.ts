import { Request, Response } from 'express';
import { getSingle } from '../../../dataServices/posts';
import { addArticle } from '../../../submodules/shared-library/services/cache/cache';
import getCategoryById from '../../../services/categories/actions/getCategoryById';
import { hive } from '../../../submodules/shared-library';

const unhidePost = async (req: Request, res: Response): Promise<Response> => {

    const { blogId, postId } = req.params;
    const populatedCategories = [];

    const article = await getSingle({_id: postId});
    const hiveArticle: any =  await hive.call('bridge', 'get_post', {author: article.username, permlink: article.permlink});
    for (const category of article.categories) {
        populatedCategories.push(await getCategoryById(category));
    }

    await addArticle(blogId, article.permlink, hiveArticle, populatedCategories);

    article.hidden = false;
    await article.save();

    return res.json({ status: 'OK', post: article });

};

export { unhidePost };
