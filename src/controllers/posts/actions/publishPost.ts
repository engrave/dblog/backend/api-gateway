import {Request, Response} from 'express';
import getAccessToken from '../../../services/vault/actions/getAccessToken';
import blogsService from '../../../services/blogs/services.blogs';
import {OperationsScope, handleHivesignerError, logger, hive} from '../../../submodules/shared-library';
import prepareOperations from '../../../services/article/actions/prepareOperations';
import sc from '../../../submodules/shared-library/services/steemconnect/hivesigner.service';
import publishedService from '../../../services/published/published.service';
import {createWithQuery, removeWithQuery} from '../../../dataServices/posts';
import {addArticle} from '../../../submodules/shared-library/services/cache/cache';
import getCategoryById from '../../../services/categories/actions/getCategoryById';
import {mockSteemArticle} from '../../../submodules/shared-library/services/article/mockSteemArticle';
import {prepareArticleFromRequest} from '../../../services/article/actions/prepareArticleFromRequest';
import {getByUsername} from '../../../dataServices/users';
import {WebpushSender} from '../../../services/WebpushSender';
import {IBlog} from '../../../submodules/shared-library/interfaces/IBlog';
import {getContentWithDelay} from '../../../submodules/shared-library/services/hive/hive';

const publishPost = async (req: Request, res: Response) => {

    const {username} = res.locals;
    const {blogId} = req.params;

    const {
        permlink,
        draftId,
        title,
        body,
        categories,
        tags,
    } = req.body;

    const accessToken = await getAccessToken(username);

    if (!accessToken) {
        throw new Error('Could not authorize user (vault is sealed)');
    }

    const blog = await blogsService.getBlogByQuery({_id: blogId});
    const user = await getByUsername(username);

    const article: any = prepareArticleFromRequest(req.body, username, blogId);

    if (!blog.sandbox) {
        await handleHivesignerError(async () => {
            const operations = await prepareOperations(article, OperationsScope.PUBLISH, blog, user);
            sc.dashboard.setAccessToken(accessToken);
            await sc.dashboard.broadcast(operations);
        });
    }

    if (draftId) {
        await removeWithQuery({_id: draftId});
    }

    const post = await createWithQuery(article);
    await publishedService.addPublishedPost(article, blog);

    const hiveContent = blog.sandbox ? mockSteemArticle(username, title, body, permlink, tags) : await getContentWithDelay(username, permlink);

    const populatedCategories = [];

    for (const category of categories) {
        populatedCategories.push(await getCategoryById(category));
    }

    await addArticle(blogId, permlink, hiveContent, populatedCategories);

    if (isOneSignalConfigured(blog)) {
        try {
            const defaultImage = blog.onesignal_logo_url ? blog.onesignal_logo_url : null;
            const imageUrl = article.thumbnail_image ? article.thumbnail_image : defaultImage;

            await WebpushSender.push(
                blog.title,
                article.title,
                `https://${blog.custom_domain ? blog.custom_domain : blog.domain}/${article.permlink}`,
                imageUrl,
                blog.onesignal_api_key,
                blog.onesignal_app_id);
        } catch (e) {
            logger.error(e);
        }
    }

    return res.json({status: 'OK', result: post});

};

const isOneSignalConfigured = (blog: IBlog): boolean => blog.onesignal_app_id && blog.onesignal_api_key && blog.onesignal_app_id !== '' && blog.onesignal_api_key !== '';

export {publishPost};
