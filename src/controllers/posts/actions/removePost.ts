import { Request, Response } from 'express';
import getAccessToken from '../../../services/vault/actions/getAccessToken';
import blogsService from '../../../services/blogs/services.blogs';
import { handleHivesignerError, OperationsScope } from '../../../submodules/shared-library';
import prepareOperations from '../../../services/article/actions/prepareOperations';
import sc from '../../../submodules/shared-library/services/steemconnect/hivesigner.service';
import { getSingle, removeWithQuery } from '../../../dataServices/posts';
import { removeArticle } from '../../../submodules/shared-library/services/cache/cache';
import { getByUsername } from '../../../dataServices/users';

const removePost = async (req: Request, res: Response) => {

    const { username } = res.locals;
    const { blogId, postId } = req.params;

    const access_token = await getAccessToken(username);

    if (!access_token) {
        throw new Error("Could not authorize user (vault is sealed)");
    }

    const blog = await blogsService.getBlogByQuery({ _id: blogId });
    const article = await getSingle({_id: postId});
    const user = await getByUsername(username);

    if (!blog.sandbox) {
        await handleHivesignerError( async () => {
            const operations = await prepareOperations(article, OperationsScope.REMOVE, blog, user);
            sc.dashboard.setAccessToken(access_token);
            await sc.dashboard.broadcast(operations);
        })
    }

    await removeWithQuery({_id: postId});
    await removeArticle(blogId, article.permlink);

    return res.json({ status: "OK" });

};

export { removePost };
