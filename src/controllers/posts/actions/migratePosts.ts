import { Request, Response } from 'express';
import blogsService from '../../../services/blogs/services.blogs';
import categoriesService from '../../../services/categories/categories.service';
import { createWithQuery } from '../../../dataServices/posts';
import { preparePostFromBlockchain, shouldBeImported } from '../../account/actions/importPosts';
import { addArticle, setBlog } from '../../../submodules/shared-library/services/cache/cache';
import { hive } from '../../../submodules/shared-library';

const migratePosts = async (req: Request, res: Response) => {

    const { posts } = req.body;

    for (const post of posts) {

        const blog = await blogsService.getBlogByQuery({ owner: post.username });

        if (blog) {

            const shouldBe = await shouldBeImported({ author: post.username, permlink: post.permlink }, post.username)

            if (shouldBe) {

                let [category] = await categoriesService.getCategoriesByQuery({ blogId: blog._id, slug: post.category })

                if (!category) {
                    console.log(" * Creating category:", post.category);
                    category = await categoriesService.createCategoryWithQuery({
                        name: post.category,
                        slug: post.category,
                        blogId: blog._id.toString()
                    });

                    const updatedBlog = await blogsService.getBlogByQuery({ _id: blog._id.toString() });
                    await setBlog(updatedBlog);
                }

                try {
                    console.log(`Importing: ${post.username}:${post.permlink}`);
                    const article = await hive.call('bridge', 'get_post', {author: post.username, permlink: post.permlink})
                    await createWithQuery(preparePostFromBlockchain(article, blog._id.toString(), category._id.toString(), post.username));
                    await addArticle(blog._id.toString(), article.permlink, article, [category]);
                } catch (error) {
                    console.log(error);
                }
            } else {
                console.log(`Already exists: ${post.username}:${post.permlink}`);
            }
        }
    }

    return res.json({ status: 'ok' });

};

export { migratePosts };
