import { Request, Response } from 'express';
import getAccessToken from '../../../services/vault/actions/getAccessToken';
import blogsService from '../../../services/blogs/services.blogs';
import { OperationsScope, handleHivesignerError, hive } from '../../../submodules/shared-library';
import prepareOperations from '../../../services/article/actions/prepareOperations';
import sc from '../../../submodules/shared-library/services/steemconnect/hivesigner.service';
import { getSingle, updateSingleWithQuery } from '../../../dataServices/posts';
import getCategoryById from '../../../services/categories/actions/getCategoryById';
import { addArticle } from '../../../submodules/shared-library/services/cache/cache';
import { prepareArticleThumbnail } from '../../../services/article/actions/prepareArticleThumbnail';
import { getByUsername } from '../../../dataServices/users';
import {client} from '../../../submodules/shared-library/services/hive/actions/engine/engine';

const editPublishedPost = async (req: Request, res: Response) => {

    const { username } = res.locals;
    const { postId, blogId } = req.params;
    const { categories, permlink, featured_image, body} = req.body;

    const accessToken = await getAccessToken(username);

    if (!accessToken) {
        throw new Error('Could not authorize user (vault is sealed)');
    }

    const blog = await blogsService.getBlogByQuery({ _id: blogId });
    const query = {
        ...req.body,
        parent_category: req.body.tags[0],
        thumbnail_image: featured_image ? featured_image : prepareArticleThumbnail(body)
    };
    await updateSingleWithQuery(postId, query);
    const article = await getSingle({_id: postId});
    const user = await getByUsername(username);

    await handleHivesignerError( async () => {
        const operations = await prepareOperations(article, OperationsScope.EDIT, blog, user);
        sc.dashboard.setAccessToken(accessToken);
        await sc.dashboard.broadcast(operations);
    });

    const hiveArticle: any = await client.call('bridge', 'get_post', {author: username, permlink});
    const populatedCategories = [];

    for (const category of categories) {
        populatedCategories.push(await getCategoryById(category));
    }

    await addArticle(blogId, permlink, hiveArticle, populatedCategories);

    return res.json({ status: 'OK', post: article});

};

export { editPublishedPost };
