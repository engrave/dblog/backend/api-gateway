import { Request, Response } from 'express';
import { getMultipleWithQueryPaginated } from '../../../dataServices/posts';
import { PostStatus } from '../../../submodules/shared-library';

const getPublished = async (req: Request, res: Response) => {

    const { blogId } = req.params;
    const { page, limit, sortBy, order} = req.query;

    const query = {
        blogId,
        status: PostStatus.PUBLISHED
    };

    const result = await getMultipleWithQueryPaginated(query, page, limit, sortBy, order);

    return res.json(result);

};

export { getPublished };