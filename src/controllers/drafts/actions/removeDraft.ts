import { Request, Response } from 'express';
import { removeWithQuery } from '../../../dataServices/posts';

const removeDraft = async (req: Request, res: Response) => {

    const { id } = req.params;
    const query = { _id: id };
    
    await removeWithQuery(query);

    return res.json({ 
        success: 'OK'
     });

};

export { removeDraft };