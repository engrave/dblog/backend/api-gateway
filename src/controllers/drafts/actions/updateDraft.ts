import { Request, Response } from 'express';
import { updateSingleWithQuery, getSingle } from '../../../dataServices/posts';
import { IPost } from '../../../submodules/shared-library/interfaces/IPost';
import { prepareArticleThumbnail } from '../../../services/article/actions/prepareArticleThumbnail';

const updateDraft = async (req: Request, res: Response) => {

    const { id } = req.params;
    const { featured_image, body} = req.body;
    const query = { _id: id };
    
    const updated = {
        ...req.body,
        thumbnail_image: featured_image ? featured_image : prepareArticleThumbnail(body),
    }

    await updateSingleWithQuery(id, updated);

    const post: IPost = await getSingle(query);

    return res.json(post);

};

export { updateDraft };