import { Request, Response } from 'express';
import { getSingle } from '../../../dataServices/posts';

const getSingleDraft = async (req: Request, res: Response) => {

    const { id } = req.params;

    const query = { _id: id }; // TODO check if it shouldn't be status draft

    const result = await getSingle(query);

    return res.json(result);

};

export { getSingleDraft };