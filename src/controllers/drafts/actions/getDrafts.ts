import { Request, Response } from 'express';
import { getMultipleWithQueryPaginated } from '../../../dataServices/posts';
import { PostStatus } from '../../../submodules/shared-library/enums/PostStatus';

const getDrafts = async (req: Request, res: Response) => {

    const { blogId } = req.params;
    const { page, limit, sortBy, order} = req.query;

    const query = {
        blogId,
        status: PostStatus.DRAFT
    };

    const result = await getMultipleWithQueryPaginated(query, page, limit, sortBy, order);

    return res.json(result);

};

export { getDrafts };