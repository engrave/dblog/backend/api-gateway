import { Request, Response } from 'express';
import { createWithQuery } from '../../../dataServices/posts';
import { IPost } from '../../../submodules/shared-library/interfaces/IPost';
import { PostStatus } from '../../../submodules/shared-library/enums/PostStatus';
import { prepareArticleThumbnail } from '../../../services/article/actions/prepareArticleThumbnail';

const createDraft = async (req: Request, res: Response) => {

    const { username } = res.locals;
        
    const {
        blogId,
        title,
        body,
        featured_image,
        scheduled,
        categories,
        decline_reward,
        tags,
        permlink
    } = req.body;

    const post: IPost = await createWithQuery({
        status: PostStatus.DRAFT,
        blogId,
        username,
        permlink,
        title,
        body,
        featured_image,
        thumbnail_image: featured_image ? featured_image : prepareArticleThumbnail(body),
        scheduled,
        categories,
        decline_reward,
        tags
    })

    return res.json(post);

};

export { createDraft };