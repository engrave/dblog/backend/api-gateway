import { Request, Response } from 'express';
import { isDomainFree } from '../../../validators/domains/isDomainFree';
import { isDomainPurchasable } from '../../../validators/domains/isDomainPurchasable';
import { DomainsManager } from '../../../services/DomainsManager';
import { DomainsDataService } from '../../../dataServices/DomainsDataService';

const cancelReservation = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params;

    await DomainsDataService.remove(id);

    return res.json({message: 'OK'});
};

export { cancelReservation };
