import { Request, Response } from 'express';
import { DomainsDataService } from '../../../dataServices/DomainsDataService';

const listDomains = async (req: Request, res: Response): Promise<Response> => {
    const { username } = res.locals;
    const domains = await DomainsDataService.getByUsername(username);
    return res.json(domains);
};

export { listDomains };
