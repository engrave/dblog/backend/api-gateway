import { Request, Response } from 'express';
import { isDomainFree } from '../../../validators/domains/isDomainFree';
import { isDomainPurchasable } from '../../../validators/domains/isDomainPurchasable';
import { DomainsManager } from '../../../services/DomainsManager';
import { DomainsDataService } from '../../../dataServices/DomainsDataService';

const reserveDomain = async (req: Request, res: Response): Promise<Response> => {
    const { username } = res.locals;
    const { domain } = req.body;

    const domainDetails = await DomainsManager.getDetails(domain);

    await isDomainPurchasable(domainDetails);

    const reservedDomain = await  DomainsDataService.create(domain, username, domainDetails.purchasePrice);

    return res.json(reservedDomain);
};

export { reserveDomain };
