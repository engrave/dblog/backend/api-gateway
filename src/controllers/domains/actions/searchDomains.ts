import { Request, Response } from 'express';
import { createWithQuery } from '../../../dataServices/posts';
import { IPost } from '../../../submodules/shared-library/interfaces/IPost';
import { PostStatus } from '../../../submodules/shared-library/enums/PostStatus';
import { prepareArticleThumbnail } from '../../../services/article/actions/prepareArticleThumbnail';
import { getCoinsPrice } from '../../../services/statistics';
import { DomainsManager } from '../../../services/DomainsManager';

const searchDomains = async (req: Request, res: Response): Promise<Response> => {

    const {keyword} = req.body;

    const details = await DomainsManager.search(keyword);

    return res.json(details);

};

export { searchDomains };
