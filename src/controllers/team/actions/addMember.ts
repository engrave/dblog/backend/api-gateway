import { Request, Response } from 'express';
import blogsService from '../../../services/blogs/services.blogs';
import { ICollaborator } from '../../../submodules/shared-library/interfaces/ICollaborator';

const addMember = async (req: Request, res: Response) => {

    const { blogId } = req.params;
    const { username, role } = req.body;
    
    const blog = await blogsService.getBlogByQuery({_id: blogId})

    const collaborator: ICollaborator = {
        username,
        role
    }

    blog.collaborators.push(collaborator);

    await blog.save();
    
    return res.json({
        status: "OK", 
        collaborators: blog.collaborators
    });

};

export { addMember };