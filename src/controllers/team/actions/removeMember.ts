import { Request, Response } from 'express';
import blogsService from '../../../services/blogs/services.blogs';
import { ICollaborator } from '../../../submodules/shared-library/interfaces/ICollaborator';
import { IBlog } from '../../../submodules/shared-library/interfaces/IBlog';

const removeMember = async (req: Request, res: Response) => {

    const { blogId } = req.params;
    const { username } = req.body;
    
    const blog: IBlog = await blogsService.getBlogByQuery({_id: blogId})

    const updatedCollaborators: ICollaborator[] = blog.collaborators.filter( member => member.username !== username);

    blog.collaborators = updatedCollaborators;

    await blog.save();
    
    return res.json({
        status: "OK", 
        collaborators: blog.collaborators
    });

};

export { removeMember };