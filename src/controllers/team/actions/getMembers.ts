import { Request, Response } from 'express';
import blogsService from '../../../services/blogs/services.blogs';

const getMembers = async (req: Request, res: Response) => {

    const { blogId } = req.params;

    const blog = await blogsService.getBlogByQuery({_id: blogId})
    
    return res.json(blog.collaborators);

};

export { getMembers };