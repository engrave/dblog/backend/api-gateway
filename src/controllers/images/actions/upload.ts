import { Request, Response } from "express";
import { uploadFromUrl, uploadFromFile } from '../../../services/image-uploader';

const uploadImage = async (req: Request, res: Response) => {
    
    let link = null;
    const { url } = req.body;
    const { file } = <any>req;

    if(url) {
        link = await uploadFromUrl(url);
    } else if(file) {
        link = await uploadFromFile(file);
    } else {
        throw new Error("Invalid request");
    }

    return res.json( {
        success: "OK",
        link: link
    });
}

export { uploadImage };